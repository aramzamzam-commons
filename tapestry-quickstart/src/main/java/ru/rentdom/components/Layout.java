package ru.rentdom.components;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.annotations.Parameter;

/**
* @author Michael Courcy
*
* This component aims to provide layout service for a page
*
*/
public class Layout {

        @Parameter(defaultPrefix=BindingConstants.LITERAL)
        private String title = "My Tapestry powered site";
        
        public String getTitle() {
         return title;
        }
        
        public void setTitle(String title) {
         this.title = title;
        }
}
