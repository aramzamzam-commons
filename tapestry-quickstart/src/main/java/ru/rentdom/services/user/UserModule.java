package ru.rentdom.services.user;

import java.util.List;

import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.ioc.services.PipelineBuilder;
import org.apache.tapestry5.services.ApplicationStateContribution;
import org.apache.tapestry5.services.ApplicationStateCreator;
import org.apache.tapestry5.services.RequestGlobals;
import org.slf4j.Logger;

import ru.rentdom.dao.UserDAO;
import ru.rentdom.entities.User;

public class UserModule {
	public static void bind(ServiceBinder binder) {
    	binder.bind(UserDAO.class);
    	binder.bind(UserService.class);
	}

	/**
	 * Строим пайп создания пользователя
	 * @param builder
	 * @param configuration
	 * @param logger
	 * @return
	 */
	public static CreateUserService buildCreateUserService(
			@InjectService("PipelineBuilder") PipelineBuilder builder, 
			List<CreateUserFilter> configuration, 
			Logger logger) {
		return builder.build(logger,CreateUserService.class, 
				CreateUserFilter.class, configuration, 
				new TerminatorImpl());
	}

	
	/**
	 * Строим пайп смены пароля
	 * @param builder
	 * @param configuration
	 * @param logger
	 * @return
	 */
	public static ChangePasswordService buildChangePasswordService(
			@InjectService("PipelineBuilder") PipelineBuilder builder, 
			List<ChangePasswordFilter> configuration, 
			Logger logger) {
		return builder.build(logger, ChangePasswordService.class, 
				ChangePasswordFilter.class, configuration, 
				new TerminatorImpl());
	}
	
	@SuppressWarnings("unchecked")
	public void contributeApplicationStateManager(MappedConfiguration<Class, ApplicationStateContribution> configuration, 
	  final RequestGlobals requestGlobals, 
	  final UserService userService)
	{
		ApplicationStateCreator<User> creator = new ApplicationStateCreator<User>()
		{
			public User create()
			{
				String username = requestGlobals.getHTTPServletRequest().getRemoteUser();
				return userService.findByUserName(username);
			}
		};
		
		configuration.add(User.class, new ApplicationStateContribution("session", creator));
	}
}
