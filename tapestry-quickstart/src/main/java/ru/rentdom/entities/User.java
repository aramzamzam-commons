package ru.rentdom.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import net.aramzamzam.commons.hibernate.ObjectSetUserType;
import net.aramzamzam.commons.hibernate.entities.BasicEntity;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.springframework.security.userdetails.UserDetails;

import ru.rentdom.utils.Authority;

@Entity(name="users")
public class User extends BasicEntity implements UserDetails{
	private static final long serialVersionUID = -8518937300513423556L;

	private String password;
	private String username;
	private String email;
	private Authority[] authorities;
	
	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	@Column
	@Type(type="net.aramzamzam.commons.hibernate.ObjectSetUserType", parameters={@Parameter(name=ObjectSetUserType.PARAMETER_CLASS, value="ru.rentdom.utils.Authority"),
																				 @Parameter(name=ObjectSetUserType.PARAMETER_IS_ARRAY, value="true")})
	public Authority[] getAuthorities() {
		return authorities;
	}
	
	public void setAuthorities(Authority[] authorities) {
		this.authorities = authorities;
	}

	@Override
	@Basic(optional = false)
	@Column(unique = true, name="username", nullable=false)
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String userName) {
		this.username=userName;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@Transient
	public boolean isEnabled() {
		return true;
	}

	@Basic(optional = false)
	@Column(unique = true, name="email", nullable=false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
