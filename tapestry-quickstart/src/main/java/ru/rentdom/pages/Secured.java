package ru.rentdom.pages;

import org.apache.tapestry5.annotations.ApplicationState;
import org.apache.tapestry5.annotations.Property;

import ru.rentdom.entities.User;

@org.springframework.security.annotation.Secured("ROLE_USER")
public class Secured {

	@SuppressWarnings("unused")
	@ApplicationState
	@Property
	private User currentUser;

}
