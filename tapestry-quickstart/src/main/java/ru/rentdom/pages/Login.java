package ru.rentdom.pages;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;

public class Login {
	
	@SuppressWarnings("unused")
	@Inject @Value("${spring-security.check.url}")
	@Property(write=false)
    private String checkUrl;

}
