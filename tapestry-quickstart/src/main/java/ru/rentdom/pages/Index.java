package ru.rentdom.pages;

import javax.servlet.http.HttpServletResponse;

import net.aramzamzam.commons.pagesbehavoir.services.HttpStatusCode;

/**
 * Start page of application rentdom-web.
 */
public class Index
{
	Object onActivate(Object context) {
		return new HttpStatusCode(HttpServletResponse.SC_NOT_FOUND, "Requested resource was not found"); 
	}

}
