package ru.rentdom.pages;

import org.apache.tapestry5.ioc.annotations.Inject;

import nu.localhost.tapestry5.springsecurity.services.LogoutService;

public class Logout {
	@Inject
	private LogoutService logoutService;
	
	Object onActivate()
	{
		logoutService.logout();
		return Index.class;
	}
}
