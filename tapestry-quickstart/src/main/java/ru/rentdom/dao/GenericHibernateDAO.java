package ru.rentdom.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.tapestry5.hibernate.HibernateSessionManager;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

/** DAO pattern from "Generic Data Access Objects": http://www.hibernate.org/328.html*/
public class GenericHibernateDAO<T, ID extends Serializable>
		implements GenericDAO<T, ID> {

	private Class<T> persistentClass;
	private final HibernateSessionManager sessionManager;

	@SuppressWarnings("unchecked")
	public GenericHibernateDAO(HibernateSessionManager sessionManager){
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		this.sessionManager = sessionManager;
	}
	
	protected Session getSession() {
		return sessionManager.getSession();
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	public List<T> findAll() {
		return findByCriteria();
	}

	@SuppressWarnings("unchecked")
	public <X extends T> List<X> findByExample(X exampleInstance, String... excludeProperty) {
		Criteria crit = getSession().createCriteria(exampleInstance.getClass());
		Example example = Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		crit.add(example);
		return crit.list();
	}

	public List<T> findByExample(T exampleInstance) {
		return findByExample(exampleInstance,new String[0]);
	}
	
	public T makePersistent(T entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	public void makeTransient(T entity) {
		getSession().delete(entity);
	}

	public void flush() {
		getSession().flush();
	}

	public void clear() {
		getSession().clear();
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}
	
	/**
	 * Возвращает null, если entity не была найдена
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		return (T) getSession().get(getPersistentClass(), id);
	}
}
