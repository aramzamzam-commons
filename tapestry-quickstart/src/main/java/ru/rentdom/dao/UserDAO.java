package ru.rentdom.dao;

import org.apache.tapestry5.hibernate.HibernateSessionManager;
import org.hibernate.criterion.Restrictions;

import ru.rentdom.entities.User;

/**
 * ДАО для работы с пользователем
 * @author solomenchuk
 *
 */
public class UserDAO extends GenericHibernateDAO<User, Long>{

	public UserDAO(HibernateSessionManager sessionManager) {
		super(sessionManager);
	}
	
	public User findByUserName(String userName)
	{
		return (User) getSession()
					.createCriteria(User.class)
					.add(Restrictions.eq("username", userName.toLowerCase()))
					.uniqueResult();
	}

	public User findByEmail(String email)
	{
		return (User) getSession()
					.createCriteria(User.class)
					.add(Restrictions.eq("email", email.toLowerCase()))
					.uniqueResult();
	}

}
