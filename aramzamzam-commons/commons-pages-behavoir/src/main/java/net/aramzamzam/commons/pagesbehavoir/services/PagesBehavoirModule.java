package net.aramzamzam.commons.pagesbehavoir.services;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.services.ComponentEventResultProcessor;
import org.apache.tapestry5.services.HttpServletRequestFilter;
import org.apache.tapestry5.services.HttpServletRequestHandler;
import org.apache.tapestry5.services.Response;

public class PagesBehavoirModule {

	@SuppressWarnings("unchecked")
	public static void contributeComponentEventResultProcessor(
			MappedConfiguration<Class, ComponentEventResultProcessor> configuration,
			final Response response) {
		configuration.add(HttpStatusCode.class,
				new ComponentEventResultProcessor<HttpStatusCode>() {
					public void processResultValue(HttpStatusCode value)
							throws IOException {
						if (!value.getLocation().isEmpty())
							response.setHeader("Location", value.getLocation());
						response.sendError(value.getStatusCode(), "");
					}
				});
	}
	
	/**
     * UTF-8 в tapestry http://wiki.apache.org/tapestry/Tapestry5Utf8Encoding
     * @param requestGlobals
     * @return
     */
    
    public HttpServletRequestFilter buildUtf8Filter()
    {
        return new HttpServletRequestFilter()
        {
			@Override
			public boolean service(HttpServletRequest request,
					HttpServletResponse response,
					HttpServletRequestHandler handler) throws IOException {
                request.setCharacterEncoding("UTF-8");
                return handler.service(request, response);
			}
        };
    }

    public void contributeHttpServletRequestHandler(
            OrderedConfiguration<HttpServletRequestFilter> configuration,
            @InjectService("Utf8Filter")
            HttpServletRequestFilter utf8Filter)
    {
        configuration.add("Utf8Filter", utf8Filter, "before:MultipartFilter");
    }
}
