package net.aramzamzam.commons.pagesbehavoir.services;

import java.io.Serializable;
import java.net.URL;

import org.apache.tapestry5.Link;

public class HttpStatusCode implements Serializable {
	private static final long serialVersionUID = 7324260894398442878L;

	private final int statusCode;
	private final String location;

	public HttpStatusCode(int statusCode) {
		this.statusCode = statusCode;
		this.location = "";
	}

	public HttpStatusCode(int statusCode, String location) {
		this.statusCode = statusCode;
		this.location = location;
	}

	public HttpStatusCode(int statusCode, Link link) {
		this(statusCode, link.toRedirectURI());
	}

	public HttpStatusCode(int statusCode, URL url) {
		this(statusCode, url.toExternalForm());
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getLocation() {
		return location;
	}
}