package net.aramzamzam.commons.components.components;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ComponentDefaultProvider;

/**
 * http://markmail.org/search/?q=list%3Aorg.apache.tapestry.users+t5+checkbox#query:list%3Aorg.apache.tapestry.users%20t5%20checkbox+page:1+mid:s7xn4o7aeuduezp2+state:results
 * <t:datacheckbox t:value="checked" t:data="data" />
 */
public class DataCheckbox extends Checkbox {

	@Parameter(required = true, allowNull = false)
	private Object _data;
	
	@SuppressWarnings("unchecked")
	@Parameter(required = true, allowNull = false)
	private ValueEncoder encoder; 

    @Inject
    private ComponentDefaultProvider defaultProvider;
    
    @Inject
    private ComponentResources resources;
	
	@Inject
	private org.apache.tapestry5.services.Request _request;

    @SuppressWarnings("unchecked")
	final ValueEncoder defaultEncoder()
    {
        return defaultProvider.defaultValueEncoder("value", resources);
    }

	
	@SuppressWarnings("unchecked")
	protected void beforeRenderTemplate(MarkupWriter writer) {
		writer.attributes("value", encoder.toClient(_data));
	}

	@Override
	protected void processSubmission(String elementName) {
		super.processSubmission(elementName);
		String value = _request.getParameter(elementName);
		if ( value != null ) {
			_data = encoder.toValue(value);
		}
	}
}