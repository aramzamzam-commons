package net.aramzamzam.commons.components.services;

import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.ObjectLocator;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.services.PropertyAccess;
import org.apache.tapestry5.services.ComponentClassTransformWorker;
import org.apache.tapestry5.services.LibraryMapping;

public class ComponentsModule {
	/**
     * Adds the CommitAfterPreparation logic, to modify processing of the 
     * {@link org.apache.tapestry5.hibernate.annotations.CommitAfter}
     * annotation according to Studynet application needs.
     */
    public static void contributeComponentClassTransformWorker(
            OrderedConfiguration<ComponentClassTransformWorker> configuration,
            PropertyAccess propertyAccess,
            ObjectLocator locator) {
        /**
         * Adds a number of standard component class transform workers:
         * <ul>
         * <li>InjectSelectionModel - generates the SelectionModel and ValueEncoder for a any marked list of objects.</li>
         * </ul>
         */

        configuration.add("InjectSelectionModel", new InjectSelectionModelWorker(propertyAccess), "after:Inject*");
    }
    
    public static void contributeComponentClassResolver(
            final Configuration<LibraryMapping> configuration) {
        configuration.add(new LibraryMapping("aramzamzam",
                "net.aramzamzam.commons.components"));
    }

}
