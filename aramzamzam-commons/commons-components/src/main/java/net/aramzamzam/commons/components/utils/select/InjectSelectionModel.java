package net.aramzamzam.commons.components.utils.select;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * В шаблоне используйте Select компонент с параметрами: get<AttributeName>SelectionModel() and get<AttributeName>ValueEncoder().
 * например:
 * java
 * @InjectSelectionModel(labelField = "name", idField = "code")
 *       private List<Bank> _list = new ArrayList<Bank>();
 * private Bank _item = null
 * 
 * tml
 * <select t:type="select" value="item"
 *             model="listSelectionModel" encoder="listValueEncoder" />
 *
 * @author solomenchuk
 *
 */
@Target(FIELD)
@Retention(RUNTIME)
@Documented
public @interface InjectSelectionModel {

        String labelField() default "null";
        String idField() default "null";
        
}