package net.aramzamzam.commons.hibernate.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.persistence.Entity;

import org.hibernate.cfg.Environment;

/**
 * Утилитный класс для операций над классами и метаданными
 * их экземпляров.
 * @author М.А. Алексеев (SVN alexeev)
 * 07.06.2008
 */
public final class ClassUtils {
	
	/**
	 * Возвращает суперкласс, который аннотирован как Entity. 
	 * Дело в том, что в списки или в отчеты могу передаваться 
	 * подклассы - DTO или еще какие-то надстройки, но при этом бывает
	 * необходимость понимать, с какой сущностью мы при этом реально работаем.
	 * @param clazz подкласс BasicEntity
	 * @return 
	 * @return clazz или один из его суперклассов, который является Hibernate Entity
	 */
	// TODO alexeev: неплохо бы тут сделать кеширование, потому что набор потенциальных
	// аргументов невелик. Тапестревое @Cached не подошло.
	@SuppressWarnings("unchecked")
	public static Class getEntityClass(Class clazz) {
		if (clazz.getAnnotation(Entity.class) != null)
			return clazz;
		Class<?> s = clazz.getSuperclass();
		if (s.getAnnotation(Entity.class) != null)
			return s;
		if (s!=null)
			return getEntityClass(clazz);
		throw new IllegalArgumentException("Class is not a successor of any annotated @Entity!");
	}
	
	/**
	 * возвращает не-проксированный класс
	 * @param proxy класс, сгенеренный проксей
	 */
	public static Class<?> stripProxy(Class<?> proxy) {
		int proxyStart = proxy.getCanonicalName().indexOf("_$$"); // javaassist
		if (proxyStart > 0) {
			try {
				return Class.forName(proxy.getCanonicalName().substring(0, proxyStart));
			} catch (ClassNotFoundException cnfe) {
				return null;
			}
		}
		proxyStart = proxy.getCanonicalName().indexOf("$$"); // cglib
		if (proxyStart > 0) {
			try {
				return Class.forName(proxy.getCanonicalName().substring(0, proxyStart));
			} catch (ClassNotFoundException cnfe) {
				return null;
			}
		}
		return proxy;
	}
	
	/**
	 * Возвращает имя хиберовской энтити по замапленному классу
	 * @param entity замапленный класс
	 * @return имя энтити либо null, если класс не замаплен
	 */
	public static String getEntityName(Class<?> entity) {
		Entity entityAnn = entity.getAnnotation(Entity.class);
		if ( entityAnn == null ) return null;
		return ( org.hibernate.cfg.BinderHelper.isDefault( entityAnn.name() ) ) ? 
				entity.getSimpleName() : entityAnn.name();
	}
	
	public static Class<?> getClassByString(String clazz)
	{
		ClassLoader[] classLoaders = new ClassLoader[]{Thread.currentThread().getContextClassLoader(), Environment.class.getClassLoader()};
		for (ClassLoader classLoader : classLoaders) {
			try {
				return classLoader.loadClass(clazz);
			} catch (ClassNotFoundException e) {
				//ignore
			}
		}
		return null;
	}
	
	
	public static <T> T createObject(Class<T> clazz)
	{
		try {
			Constructor<T> c = clazz.getConstructor();
			return c.newInstance();
		} catch (SecurityException e) {
			return null;
		} catch (IllegalArgumentException e) {
			return null;
		} catch (NoSuchMethodException e) {
			return null;
		} catch (InstantiationException e) {
			return null;
		} catch (IllegalAccessException e) {
			return null;
		} catch (InvocationTargetException e) {
			return null;
		}
	}

}
