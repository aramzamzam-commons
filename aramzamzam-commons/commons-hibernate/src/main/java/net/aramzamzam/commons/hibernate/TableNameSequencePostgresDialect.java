package net.aramzamzam.commons.hibernate;


import org.hibernate.dialect.PostgreSQLDialect;

public class TableNameSequencePostgresDialect extends PostgreSQLDialect {

	/**
     * Get the native identifier generator class.
     * @return TableNameSequenceGenerator.
     */
    @Override
    public Class<?> getNativeIdentifierGeneratorClass() {
            return TableNameSequenceGenerator.class;
    }

	
}
