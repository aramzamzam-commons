package net.aramzamzam.commons.hibernate;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

import net.aramzamzam.commons.hibernate.utils.ClassUtils;
import net.aramzamzam.commons.hibernate.utils.StringUtils;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

/**
 * Пользовательский тип для хранения коллекции строк в строке 
 * @author solomenchuk
 */
public class ObjectSetUserType implements UserType, ParameterizedType{
	private static final int[] TYPES = { Types.CLOB };
	public static final String PARAMETER_DELIMITER="delimiter";
	public static final String PARAMETER_CLASS="class";
	public static final String PARAMETER_IS_ARRAY="isarray";
	private String DELIMITER=";";
	private Class<?> CLASS=String.class;
	private boolean IS_ARRAY=false;


	@Override
	public Object assemble(Serializable value, Object arg1)
			throws HibernateException {
		return deepCopy(value);
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) deepCopy(value);
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x==y) return true;
		if (x==null) return  y==null;
		return x.equals(y);
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	@Override
	public boolean isMutable() {
		return false;
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, Object arg2)
			throws HibernateException, SQLException {
            String value = rs.getString(names[0]);
            if ( rs.wasNull() ) return null;
            int dl=DELIMITER.length();
            String v=value.substring(dl,value.length()-dl);
            if (IS_ARRAY)
            	return StringUtils.toArray(v, CLASS, DELIMITER, false);
            else
            	return StringUtils.toCollection(v, CLASS, DELIMITER, false);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index)
			throws HibernateException, SQLException {
		if ( value == null ) {
			Hibernate.STRING.nullSafeSet(st, value, index);
        }
        else {
        	Iterable<?>v;
        	if (value.getClass().isArray())
        	{
        		v=new ArrayList<Object>();
        		Object[] o = (Object[])value;
        		for (Object x: o)
        			((ArrayList)v).add(x);
        	}
        	else
        		v=(Iterable<?>) value;
        	Hibernate.STRING.nullSafeSet(st, DELIMITER+StringUtils.toString(v, DELIMITER)+DELIMITER, index);
        }
	}

	@Override
	public Object replace(Object x, Object arg1, Object arg2)
			throws HibernateException {
		return deepCopy(x);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class returnedClass() {
		return Set.class;
	}

	@Override
	public int[] sqlTypes() {
		return TYPES;
	}

	@Override
	public void setParameterValues(Properties parameters) {
		if (parameters==null)
			return;
		String d = parameters.getProperty(PARAMETER_DELIMITER);
		if (d!=null)
			DELIMITER=d;
		Boolean b = new Boolean(parameters.getProperty(PARAMETER_IS_ARRAY));
		IS_ARRAY=b;
		
		String s = parameters.getProperty(PARAMETER_CLASS);
		if (s!=null)
		{
			CLASS=ClassUtils.getClassByString(s);
			if (CLASS==null)
				throw new RuntimeException("Unable to load class: "+s);
		}
	}
}
