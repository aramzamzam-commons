package net.aramzamzam.commons.hibernate.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import net.aramzamzam.commons.hibernate.utils.AssociationExample;

import org.apache.tapestry5.hibernate.HibernateSessionManager;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

/** DAO pattern from "Generic Data Access Objects": http://www.hibernate.org/328.html*/
public class GenericHibernateDAO<T, ID extends Serializable>
		implements GenericDAO<T, ID> {

	private Class<T> persistentClass;
	private final HibernateSessionManager sessionManager;

	@SuppressWarnings("unchecked")
	public GenericHibernateDAO(HibernateSessionManager sessionManager){
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		this.sessionManager = sessionManager;
	}
	
	protected Session getSession() {
		return sessionManager.getSession();
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	public List<T> findAll() {
		return findByCriteria();
	}

	/**
	 * Finds list of entities by example
	 * @param <X>
	 * 	entity class
	 * @param exampleInstance
	 * 	entity instance
	 * @param includeAssociations
	 * 	include associations in search
	 * @param excludeProperty
	 * 	array of excluded properties
	 * @return
	 * 	list of founded entites (nulls and zero properties are expluded)
	 */
	@SuppressWarnings("unchecked")
	public <X extends T> List<X> findByExample(X exampleInstance, boolean includeAssociations,  String... excludeProperty) {
		Criteria crit = getSession().createCriteria(exampleInstance.getClass());
		Criterion example;
		if (includeAssociations)
		{
			AssociationExample e = AssociationExample.create(exampleInstance);
			e.setIncludeAssociations(true);
			for (String exclude : excludeProperty)
				e.excludeProperty(exclude);
			example=e;
		}
		else
		{
			Example e = Example.create(exampleInstance);
			for (String exclude : excludeProperty)
				e.excludeProperty(exclude);
			example=e;
		}
		crit.add(example);
		return crit.list();
	}

	public <X extends T> List<X> findByExample(X exampleInstance) {
		return findByExample(exampleInstance,false, new String[0]);
	}
	
	public T makePersistent(T entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	public void makeTransient(T entity) {
		getSession().delete(entity);
	}

	public void flush() {
		getSession().flush();
	}

	public void clear() {
		getSession().clear();
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}
	
	/**
	 * returns null, if entity was not found
	 * @param id
	 * 	entity id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		return (T) getSession().get(getPersistentClass(), id);
	}
	
	/**
	 * returns null, if entity was not found
	 * @param id
	 * 	entity id
	 * @param entityClass
	 * 	entity class
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <X extends T> X findById(ID id, Class<X> entityClass) {
		return (X) getSession().get(entityClass, id);
	}

}
