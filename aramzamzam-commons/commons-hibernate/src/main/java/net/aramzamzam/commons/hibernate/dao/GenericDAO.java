package net.aramzamzam.commons.hibernate.dao;

import java.io.Serializable;
import java.util.List;
/** DAO pattern from "Generic Data Access Objects": http://www.hibernate.org/328.html*/
public interface GenericDAO<T, ID extends Serializable> {

    T findById(ID id);

    List<T> findAll();

    public <X extends T> List<X> findByExample(X exampleInstance);

    T makePersistent(T entity);

    void makeTransient(T entity);
}
