package net.aramzamzam.commons.hibernate.utils;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public final class StringUtils {

	/**
	 * Преобразование строки в разделителями в {@link Collection}
	 * 
	 * @param input
	 *            строка
	 * @param delimiter
	 *            разделитель
	 * @param trim
	 *            тримить подстроку?
	 * @param clazz
	 *            класс обьекта
	 * @return
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public static <T> Collection<T> toCollection(String input, Class<T> clazz, String delimiter, boolean trim) {
		try {
			Constructor<T> constructor = clazz.getConstructor(String.class);
			if (constructor == null)
				throw new NoSuchMethodError("Constructor " + clazz.getCanonicalName() + "(String s) undefined");
			if (input == null)
				return Collections.emptyList();
			String[] arr = input.split(delimiter);
			Collection<T> result = new LinkedList<T>();
			for (String s : arr) {
				T v = (T) constructor.newInstance(trim ? s.trim() : s);
				result.add(v);
			}
			return result;
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	};
	
	@SuppressWarnings("unchecked")
	public static <T> T[] toArray(String input, Class<T> clazz, String delimiter, boolean trim) {
		try {
			Constructor<T> constructor = clazz.getConstructor(String.class);
			if (constructor == null)
				throw new NoSuchMethodError("Constructor " + clazz.getCanonicalName() + "(String s) undefined");
			if (input == null)
				return (T[]) Array.newInstance(clazz, 0);
			
			String[] arr = input.split(delimiter);
			
			LinkedList<T> result = new LinkedList<T>();
			
			for (String s : arr) {
				T v = (T) constructor.newInstance(trim ? s.trim() : s);
				result.add(v);
			}
			return result.toArray((T[]) Array.newInstance(clazz, result.size()));
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	};

	/**
	 * Преобразует {@link Collection} в строку с разделителями
	 * 
	 * @param input
	 *            Set
	 * @param delimiter
	 *            разделитель
	 * @return
	 */
	public static String toString(Iterable<?> input, String delimiter) {
		StringBuilder bulder = new StringBuilder();
		for (Object o : input)
			bulder.append(o.toString() + delimiter);

		return bulder.substring(0, bulder.length() - delimiter.length());
	}

	/**
	 * Возвращает имя пользователя по почтовому адресу
	 * 
	 * @param email
	 *            адрес в сокращенном виде username@domain.com
	 * @return username, или "" если адрес не валидный
	 */
	public static String getUsernameFromEmail(final String email) {
		if (email == null || email.length() < 5)
			return "";

		int alhebPos = email.indexOf('@');
		if (alhebPos < 1)
			return "";

		String username = email.substring(0, alhebPos);
		return username != null ? username : "";
	}

	/**
	 * Удаляет все xml- или html-теги из строки, заменяя участки "<...>"
	 * пробелами
	 * 
	 * @param source
	 * @return
	 */
	public static String stripTags(final String source) {
		if (source == null)
			return source;

		StringBuffer result = new StringBuffer(source.length());
		for (String text : source.split("<[^<>]*>")) {
			if (!"".equals(text)) {
				result.append(text);
				result.append(" ");
			}
		}
		return result.toString();
	}

	/**
	 * Форматирует размер (файла) в виде "15 байт", "20,5 Кбайт", "14,35 Мбайт"
	 * 
	 * @param bytes
	 *            длина в байтах
	 * @param precision
	 *            до скольки знаков округлять
	 * @return
	 */
	public static String formatSize(long bytes, int precision) {
		return bytes + " байт";
	}

	/**
	 * проверяет, валидный ли email. регулярное выражение - стандарт RFC 2822
	 * 
	 * @param email
	 * @return <code>true</code>, если email валидный
	 */
	public static boolean isEmailValid(String email) {
		if (email.length() <= 50
				&& email
						.matches("(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"))
			return true;
		else
			return false;
	}

	public static boolean isNickNameValid(String nickName) {
		if (nickName.matches("[A-Za-z0-9-_]{5,20}"))
			return true;
		else
			return false;
	}

}
