package net.aramzamzam.commons.hibernate.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author James Carman
 */
@SuppressWarnings("unchecked")
@MappedSuperclass
public class BasicEntity implements Serializable, Comparable {
	private static final long serialVersionUID = 1L;
	protected Long id;
    
    public int compareTo(Object o) {
        return String.valueOf(this).compareTo(String.valueOf(o));
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    // FIXME доступ к этому методу следует сделать private
    public void setId(Long id) {
        this.id = id;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
    //see http://roald.jteam.nl/?p=25
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || 
					!(
							getClass() == obj.getClass() 
							|| getClass().getSuperclass() == obj.getClass().getSuperclass()
							|| getClass() == obj.getClass().getSuperclass()
							|| getClass().getSuperclass() == obj.getClass()
					))
			return false;
		final BasicEntity other = (BasicEntity) obj;
		if (id != null ? !id.equals(other.getId()) : other.getId() != null)
			return false;
		return true;
	}
    
}