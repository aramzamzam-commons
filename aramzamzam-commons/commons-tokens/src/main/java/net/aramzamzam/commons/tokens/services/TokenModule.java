package net.aramzamzam.commons.tokens.services;

import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;

public class TokenModule {

	public static void bind(ServiceBinder binder)
    {
        binder.bind(TokenManager.class);
    }
	
	public void contributeFactoryDefaults(MappedConfiguration<String, String> configuration)
	  {
	      configuration.add("net.aramzamzam.commons.tokens.salt", "VOVASTY");
	  }
}
